package bp.wf.data;
import bp.en.EntityNoNameAttr;

/** 
 常用语属性
*/
public class FastInputAttr extends EntityNoNameAttr
{
	/** 
	 类型
	*/
	public static final String ContrastKey = "ContrastKey";
	/** 
	 Vals
	*/
	public static final String Vals = "Vals";
	/** 
	 流程编号
	*/
	public static final String FK_Emp = "FK_Emp";
	/** 
	 顺序号
	*/
	public static final String Idx = "Idx";
}